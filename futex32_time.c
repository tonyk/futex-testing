/*
 * futex_time
 *
 * This program tests different clockids for futex_wait timeout.
 * Beware of y2038 hacks. There is no interface for 32 bit time.
 *
 * Author: André Almeida <andrealmeid@collabora.com>
 * License: GPLv2
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include <time.h>
#include <linux/unistd.h>

/*
 * - Y2038 section for 32-bit applications -
 *
 * Remove this when glibc is ready for y2038. Then, always compile with
 * `-DTIME_BITS=64` or `-D__USE_TIME_BITS64`. glibc will provide both
 * timespec64 and clock_gettime64 so we won't need to define here.
 */
#if __TIMESIZE == 32
# define NR_gettime __NR_clock_gettime64
#else
# define NR_gettime __NR_clock_gettime
#endif

struct timespec64 {
         long long tv_sec;        /* seconds */
         long long tv_nsec;       /* nanoseconds */
};

int gettime(clock_t clockid, struct timespec64 *tv)
{
	return syscall(NR_gettime, clockid, tv);
}
/*
 * - End of Y2038 section -
 */

// syscall numbers
#undef __NR_futex_wait
#undef __NR_futex_wake
#define __NR_futex_wait 440
#define __NR_futex_wake 441

// futex flags
#define FUTEX_CLOCKRT		256
#define FUTEX_SIZE_32 2

#define futex32 uint32_t

/*
 * wait for uaddr if (*uaddr == val)
 */
static int futex_wait(void *uaddr, unsigned long val, unsigned long flags,
		      struct timespec64 *timo)
{
	return syscall(__NR_futex_wait, uaddr, val, flags, timo);
}

/*
 * Test a waiter with timeout
 */
void run_timeout(clock_t clockid)
{
	struct timespec64 tv;
	futex32 f = 0;
	int ret;
	int flags = FUTEX_SIZE_32;

	if (clockid == CLOCK_REALTIME)
		flags |= FUTEX_CLOCKRT;

	printf("\nTesting %s timeout\n", clockid == CLOCK_REALTIME ? "realtime" : "monotonic");

	if (gettime(clockid, &tv)) {
		printf("gettime failed\n");
		exit(1);
	}

	printf("current time: %lld secs\n", tv.tv_sec);

	tv.tv_sec += 2;

	printf("will timeout at: %lld secs\n", tv.tv_sec);

	ret = futex_wait(&f, 0, flags, &tv);

	// wait for it...

	if (ret == -1 && errno == ETIMEDOUT)
		printf("OK\n");
	else
		printf("ERROR ret %d errno %d\n", ret, errno);
}

int main()
{
	run_timeout(CLOCK_REALTIME);
	run_timeout(CLOCK_MONOTONIC);

	return 0;
}
