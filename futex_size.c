/*
 * futex_sizes
 * This program tests different size configurations for futex, using the
 * variable-size futex feature.
 *
 * Author: André Almeida <andrealmeid@collabora.com>
 * License: GPLv2
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include <time.h>
#include <syscall.h>
#include <linux/futex.h>

// syscall numbers
//#define __NR_futex_wait 439
//#define __NR_futex_wake 440

#define NSEC 1000000000

// wait time before triggering a wake up
#define WAKE_WAIT_US 100000

#define FUTEX_PRIVATE_FLAG	128
#define FUTEX_CLOCKRT	256
#define FUTEX_CMD_MASK		~(FUTEX_PRIVATE_FLAG | FUTEX_CLOCK_REALTIME)

// flags for syscalls, defining the size
#define FUTEX_SIZE_8  FUTEX_8
#define FUTEX_SIZE_16 FUTEX_16
#define FUTEX_SIZE_32 FUTEX_32 
#define FUTEX_SIZE_64 FUTEX_64

#define FUTEX_PRIVATE_SIZE_8  (FUTEX_SIZE_8  | FUTEX_PRIVATE_FLAG)
#define FUTEX_PRIVATE_SIZE_16 (FUTEX_SIZE_16 | FUTEX_PRIVATE_FLAG)
#define FUTEX_PRIVATE_SIZE_32 (FUTEX_SIZE_32 | FUTEX_PRIVATE_FLAG)
#define FUTEX_PRIVATE_SIZE_64 (FUTEX_SIZE_64 | FUTEX_PRIVATE_FLAG)

// available sizes for futex
#define futex8  uint8_t
#define futex16 uint16_t
#define futex32 uint32_t
#define futex64 uint64_t

#define SUCCESS 0
#define ERROR -1

struct results {
	unsigned int pass;
	unsigned int error;
};

// edge case values, to test sizes
#define VALUE16 257        // 2^8  + 1
#define VALUE32 65537      // 2^16 + 1
#define VALUE64 4294967297 // 2^32 + 1

/*
 * uaddr: futex address
 * flags: flags for futex (mainly size flags)
 * val: expected value
 */
struct futex {
	void *uaddr;
	unsigned long flags;
	unsigned long val;
};

/*
 * wait for uaddr if (*uaddr == val)
 */
static int futex_wait(void *uaddr, unsigned long val, unsigned long flags,
		      time_t *timo)
{
	return syscall(__NR_futex_wait, uaddr, val, flags, timo);
}

/*
 * wake nr futexes waiting for uaddr
 */
static int futex_wake(void *uaddr, unsigned int nr, unsigned long flags)
{
	return syscall(__NR_futex_wake, uaddr, nr, flags);
}

/*
 * wait in  f->uaddr
 */
void *waiterfn(void *arg)
{
	int ret;

	struct futex *f = (struct futex *) arg;

	ret = futex_wait(f->uaddr, f->val, f->flags, NULL);
	if (ret == ERROR)
		printf("waiter failed %d errno %d\n", ret, errno);

	return NULL;
}

/*
 * wait and expect a EAGAIN return
 */
void waiterfn_again(struct futex f, struct results *r)
{
	int ret;

	printf("Testing wouldblock... ");

	ret = futex_wait(f.uaddr, f.val, f.flags, NULL);
	if (ret == ERROR && errno == EAGAIN) {
		printf("OK\n");
		r->pass += 1;
	} else {
		printf("ERROR ret %d errno %d\n", ret, errno);
		r->error += 1;
	}

}

/*
 * wait and expect a EINVAL return
 */
void waiterfn_inval(struct futex f, struct results *r)
{
	int ret;

	printf("Testing invalid... ");

	ret = futex_wait(f.uaddr, f.val, f.flags, NULL);
	if (ret == ERROR && errno == EINVAL) {
		printf("OK\n");
		r->pass += 1;
	} else {
		printf("ERROR ret %d errno %d\n", ret, errno);
		r->error += 1;
	}
}

/*
 * create a thread to wait, then wake it
 */
void test_single_waiter(struct futex f, struct results *r)
{
	pthread_t waiter;
	int ret;

	pthread_create(&waiter, NULL, waiterfn, &f);

	usleep(WAKE_WAIT_US);

	printf("Testing a single waiter... ");
	ret = futex_wake(f.uaddr, 1, f.flags);
	if (ret == 1) {
		printf("OK\n");
		r->pass += 1;
	} else {
		printf("ERROR ret %d errno %d\n", ret, errno);
		r->error += 1;
	}
}

/*
 * create a waiter that we know will fail. wait for a timeout
 */
void *waiterfn_timeout(void *arg)
{
	struct timespec tv;
	time_t timo, tnow;
	int ret;
	struct futex *f = (struct futex *) arg;

	int *p_ret = malloc(sizeof(int));

	clock_gettime(CLOCK_MONOTONIC, &tv);
	tnow = NSEC * tv.tv_sec + tv.tv_nsec;
	timo = tnow + ((unsigned long) NSEC * 2);

	printf("\nTesting timeout (%.3f seconds)... ", (float) (timo - tnow) / NSEC);

	ret = futex_wait(f->uaddr, 0, f->flags, &timo);
	if (ret != -1 && errno != ETIMEDOUT) {
		printf("ERROR ret %d errno %d", ret, errno);
	}

	printf("\n");
	*p_ret = errno;
	pthread_exit(p_ret);
}

/*
 * Test for simple wait/wake mechanism
 * Wait expecting value 0
 */
void run_simple_wait(struct results *r)
{
	struct futex f;
	f.val = 0;
	futex8  f8 = 0;
	futex16 f16 = 0;
	futex32 f32 = 0;
	futex64 f64 = 0;

	printf("size = 8\n");
	f.uaddr = &f8;
	f.flags = FUTEX_PRIVATE_SIZE_8;
	test_single_waiter(f, r);

	printf("\nsize = 16\n");
	f.uaddr = &f16;
	f.flags = FUTEX_PRIVATE_SIZE_16;
	test_single_waiter(f, r);

	printf("\nsize = 32\n");
	f.uaddr = &f32;
	f.flags = FUTEX_PRIVATE_SIZE_32;
	test_single_waiter(f, r);

	printf("\nsize = 64\n");
	f.uaddr = &f64;
	f.flags = FUTEX_PRIVATE_SIZE_64;
	test_single_waiter(f, r);
}

/*
 * Test for using wrong size flags.
 *
 * In case one use a flag bigger than the futex, it should return -EINVAL,
 * since it will try to access a region bigger than the variable
 *
 */

void run_bigger_flag(struct results *r)
{
	struct futex f;
	f.val = 0;
	futex8  f8 = 0;
	futex16 f16 = 0;
	futex32 f32 = 0;

	/*
	 * Testing for using sizes bigger than the futex
	 */

	printf("\nsize 8 flag 64\n");
	f.uaddr = &f8;
	f.flags = FUTEX_PRIVATE_SIZE_64;

	waiterfn_inval(f, r);

	printf("\nsize 8 flag 32\n");
	f.uaddr = &f8;
	f.flags = FUTEX_PRIVATE_SIZE_32;

	waiterfn_inval(f, r);

	printf("\nsize 8 flag 16\n");
	f.uaddr = &f8;
	f.flags = FUTEX_PRIVATE_SIZE_16;

	waiterfn_inval(f, r);

	printf("\nsize 16 flag 32\n");
	f.uaddr = &f16;
	f.flags = FUTEX_PRIVATE_SIZE_32;

	waiterfn_inval(f, r);

	printf("\nsize 16 flag 64\n");
	f.uaddr = &f16;
	f.flags = FUTEX_PRIVATE_SIZE_64;

	waiterfn_inval(f, r);

	// needs some investigation ...
	printf("\nsize 32 flag 64\n");
	f.uaddr = &f32;
	f.flags = FUTEX_PRIVATE_SIZE_64;

	waiterfn_inval(f, r);
}

/*
 * In case one use a flag smaller than the futex size, it should return -EAGAIN,
 * since it will access a smaller memory region and use just a small portion
 * of the number.
 */
void run_smaller_flag(struct results *r)
{
	struct futex f;
	futex16 f16 = VALUE16;
	futex32 f32 = VALUE32;
	futex64 f64 = VALUE64;

	/*
	 * Testing for using sizes smaller than the futex
	 */

	printf("\nsize 16 flag 8\n");
	f.val = VALUE16;
	f.uaddr = &f16;
	f.flags = FUTEX_PRIVATE_SIZE_8;
	waiterfn_again(f, r);

	printf("\nsize 32 flag 8\n");
	f.val = VALUE32;
	f.uaddr = &f32;
	f.flags = FUTEX_PRIVATE_SIZE_8;
	waiterfn_again(f, r);

	printf("\nsize 64 flag 8\n");
	f.val = VALUE64;
	f.uaddr = &f64;
	f.flags = FUTEX_PRIVATE_SIZE_8;
	waiterfn_again(f, r);

	printf("\nsize 32 flag 16\n");
	f.val = VALUE32;
	f.uaddr = &f32;
	f.flags = FUTEX_PRIVATE_SIZE_16;
	waiterfn_again(f, r);

	printf("\nsize 64 flag 16\n");
	f.val = VALUE64;
	f.uaddr = &f64;
	f.flags = FUTEX_PRIVATE_SIZE_16;
	waiterfn_again(f, r);

	printf("\nsize 64 flag 32\n");
	f.val = VALUE64;
	f.uaddr = &f64;
	f.flags = FUTEX_PRIVATE_SIZE_32;
	waiterfn_again(f, r);
}

/*
 * Tests if each size of futex can correctly handle futexes of their sizes
 */
void run_correct_flag(struct results *r)
{
	struct futex f;
	futex16 f16 = VALUE16;
	futex32 f32 = VALUE32;
	futex64 f64 = VALUE64;

	printf("\nsize 16\n");
	f.val = VALUE16;
	f.uaddr = &f16;
	f.flags = FUTEX_PRIVATE_SIZE_16;

	test_single_waiter(f, r);

	printf("\nsize 32\n");
	f.val = VALUE32;
	f.uaddr = &f32;
	f.flags = FUTEX_PRIVATE_SIZE_32;

	test_single_waiter(f, r);

	printf("\nsize 64\n");
	f.val = VALUE64;
	f.uaddr = &f64;
	f.flags = FUTEX_PRIVATE_SIZE_64;

	test_single_waiter(f, r);
}

/*
 * Test a waiter with timeout
 */
void run_timeout(struct results *r)
{
	struct timespec tv;
	time_t timo;
	time_t tnow;
	futex32 f = 0;
	int ret;


	clock_gettime(CLOCK_REALTIME, &tv);
	tnow = NSEC * tv.tv_sec + tv.tv_nsec;

	timo = tnow + ((unsigned long) NSEC * 2);

	printf("\nTesting realtime timeout (%.3f seconds)... ", (float) (timo - tnow) / NSEC);

	ret = futex_wait(&f, 0, FUTEX_PRIVATE_SIZE_32 | FUTEX_CLOCKRT, &timo);
	if (ret == -1 && errno == ETIMEDOUT) {
		printf("OK\n");
		r->pass += 1;
	} else {
		printf("ERROR ret %d errno %d\n", ret, errno);
		r->error += 1;
	}


	clock_gettime(CLOCK_MONOTONIC, &tv);
	tnow = NSEC * tv.tv_sec + tv.tv_nsec;

	timo = tnow + ((unsigned long) NSEC * 2);

	printf("\nTesting monotic timeout (%.3f seconds)... ", (float) (timo - tnow) / NSEC);

	ret = futex_wait(&f, 0, FUTEX_PRIVATE_SIZE_32, &timo);
	if (ret == -1 && errno == ETIMEDOUT) {
		printf("OK\n");
		r->pass += 1;
	} else {
		printf("ERROR ret %d errno %d\n", ret, errno);
		r->error += 1;
	}
}

/*
 * Test sharead futexes
 * The other tests uses privates ones
 */
void run_shared_futex(struct results *r)
{
	struct futex f;
	f.val = 0;
	futex8  f8 = 0;
	futex16 f16 = 0;
	futex32 f32 = 0;
	futex64 f64 = 0;

	printf("size = 8\n");
	f.uaddr = &f8;
	f.flags = FUTEX_SIZE_8;
	test_single_waiter(f, r);

	printf("\nsize = 16\n");
	f.uaddr = &f16;
	f.flags = FUTEX_SIZE_16;
	test_single_waiter(f, r);

	printf("\nsize = 32\n");
	f.uaddr = &f32;
	f.flags = FUTEX_SIZE_32;
	test_single_waiter(f, r);

	printf("\nsize = 64\n");
	f.uaddr = &f64;
	f.flags = FUTEX_SIZE_64;
	test_single_waiter(f, r);
}

/*
 * test waiting in a overlapping futex
 *
 * if you sleep using size X in addr A, you can't wake in addr A using size Y
 * (given X != Y)
 */
void test_overlapping(struct results *r)
{
	struct futex f;
	f.val = 0;
	futex16 f16 = 0;
	futex32 f32 = 0;
	futex64 f64 = 0;
	pthread_t waiter;
	void *p_ret;
	int ret, num;

	printf("testing overlapping\n");

	/* wait using 16, waking using 8 */
	f.uaddr = &f16;
	f.flags = FUTEX_PRIVATE_SIZE_16;

	pthread_create(&waiter, NULL, waiterfn_timeout, &f);

	usleep(WAKE_WAIT_US);

	f.flags = FUTEX_PRIVATE_SIZE_8;

	ret = futex_wake(f.uaddr, 1, f.flags);

	pthread_join(waiter, (void **) &p_ret);

	num = *((int *) p_ret);

	if (ret == 0 && num == ETIMEDOUT) {
		printf("OK\n");
		r->pass += 1;
	} else {
		printf("ERROR ret errno %d errno real %d\n", num, errno);
		r->error += 1;
	}
	free(p_ret);

	/* wait using 64, waking using 16 */
	f.uaddr = &f64;
	f.flags = FUTEX_PRIVATE_SIZE_64;

	pthread_create(&waiter, NULL, waiterfn_timeout, &f);

	usleep(WAKE_WAIT_US);

	f.flags = FUTEX_PRIVATE_SIZE_16;

	ret = futex_wake(f.uaddr, 1, f.flags);

	pthread_join(waiter, &p_ret);
	num = *((int *) p_ret);

	if (ret == 0 && num == ETIMEDOUT) {
		printf("OK\n");
		r->pass += 1;
	} else {
		printf("ERROR ret %d errno %d errno real %d\n", ret, num, errno);
		r->error += 1;
	}

	free(p_ret);

	/* wait using 32, waking using 16 */

	f.uaddr = &f32;
	f.flags = FUTEX_PRIVATE_SIZE_32;

	pthread_create(&waiter, NULL, waiterfn_timeout, &f);

	usleep(WAKE_WAIT_US);

	f.flags = FUTEX_PRIVATE_SIZE_16;

	ret = futex_wake(f.uaddr, 1, f.flags);

	pthread_join(waiter, &p_ret);
	num = *((int *) p_ret);

	if (ret == 0 && num == ETIMEDOUT) {
		printf("OK\n");
		r->pass += 1;
	} else {
		printf("ERROR ret %d errno %d errno real %d\n", ret, num, errno);
		r->error += 1;
	}

	free(p_ret);
}

int main()
{
	struct results results;
	results.pass = 0;
	results.error = 0;

	run_simple_wait(&results);
	run_correct_flag(&results);
	run_timeout(&results);
	run_shared_futex(&results);

	run_bigger_flag(&results);
	run_smaller_flag(&results);

	test_overlapping(&results);

	printf("\nSummary: %u pass %u error (total %u)\n", results.pass,
	       results.error, results.pass + results.error);

	return 0;
}
