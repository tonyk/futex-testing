/*
 * futex_sizes
 * This program tests different size configurations for futex, using the
 * variable-size futex feature.
 *
 * Author: André Almeida <andrealmeid@collabora.com>
 * License: GPLv2
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include <time.h>
#include <linux/unistd.h>

// syscall numbers
/*
#define __NR_futex_wait 439
#define __NR_futex_wake 440
*/

#define NSEC 1000000000

// wait time before triggering a wake up
#define WAKE_WAIT_US 100000

#define FUTEX_PRIVATE_FLAG	128
#define FUTEX_CLOCKRT	256
#define FUTEX_CMD_MASK		~(FUTEX_PRIVATE_FLAG | FUTEX_CLOCK_REALTIME)

// flags for syscalls, defining the size
#define FUTEX_SIZE_8  0
#define FUTEX_SIZE_16 1
#define FUTEX_SIZE_32 2
#define FUTEX_SIZE_64 3

#define FUTEX_PRIVATE_SIZE_8  (FUTEX_SIZE_8  | FUTEX_PRIVATE_FLAG)
#define FUTEX_PRIVATE_SIZE_16 (FUTEX_SIZE_16 | FUTEX_PRIVATE_FLAG)
#define FUTEX_PRIVATE_SIZE_32 (FUTEX_SIZE_32 | FUTEX_PRIVATE_FLAG)
#define FUTEX_PRIVATE_SIZE_64 (FUTEX_SIZE_64 | FUTEX_PRIVATE_FLAG)

// available sizes for futex
#define futex8  uint8_t
#define futex16 uint16_t
#define futex32 uint32_t
#define futex64 uint64_t

#define SUCCESS 0
#define ERROR -1

struct results {
	unsigned int pass;
	unsigned int error;
};

// edge case values, to test sizes
#define VALUE16 257        // 2^8  + 1
#define VALUE32 65537      // 2^16 + 1
#define VALUE64 4294967297 // 2^32 + 1

/*
 * uaddr: futex address
 * flags: flags for futex (mainly size flags)
 * val: expected value
 */
struct futex {
	void *uaddr;
	unsigned long flags;
	unsigned long val;
};

/*
 * wait for uaddr if (*uaddr == val)
 */
static int futex_wait(void *uaddr, unsigned long val, unsigned long flags,
		      time_t *timo)
{
	return syscall(__NR_futex_wait, uaddr, val, flags, timo);
}

/*
 * wake nr futexes waiting for uaddr
 */
static int futex_wake(void *uaddr, unsigned int nr, unsigned long flags)
{
	return syscall(__NR_futex_wake, uaddr, nr, flags);
}

/*
 * wait in  f->uaddr
 */
void *waiterfn(void *arg)
{
	int ret;

	struct futex *f = (struct futex *) arg;

	ret = futex_wait(f->uaddr, f->val, f->flags, NULL);
	if (ret == ERROR)
		printf("waiter failed %d errno %d\n", ret, errno);

	return NULL;
}

/*
 * wait and expect a EAGAIN return
 */
void waiterfn_again(struct futex f, struct results *r)
{
	int ret;

	printf("Testing wouldblock... ");

	ret = futex_wait(f.uaddr, f.val, f.flags, NULL);
	if (ret == ERROR && errno == EAGAIN) {
		printf("OK\n");
		r->pass += 1;
	} else {
		printf("ERROR ret %d errno %d\n", ret, errno);
		r->error += 1;
	}

}

/*
 * wait and expect a EINVAL return
 */
void waiterfn_inval(struct futex f, struct results *r)
{
	int ret;

	printf("Testing invalid... ");

	ret = futex_wait(f.uaddr, f.val, f.flags, NULL);
	if (ret == ERROR && errno == EINVAL) {
		printf("OK\n");
		r->pass += 1;
	} else {
		printf("ERROR ret %d errno %d\n", ret, errno);
		r->error += 1;
	}
}

/*
 * create a thread to wait, then wake it
 */
void test_single_waiter(struct futex f, struct results *r)
{
	pthread_t waiter;
	int ret;

	pthread_create(&waiter, NULL, waiterfn, &f);

	usleep(WAKE_WAIT_US);

	printf("Testing a single waiter... ");
	ret = futex_wake(f.uaddr, 1, f.flags);
	if (ret == 1) {
		printf("OK\n");
		r->pass += 1;
	} else {
		printf("ERROR ret %d errno %d\n", ret, errno);
		r->error += 1;
	}
}

/*
 * create a waiter that we know will fail. wait for a timeout
 */
void *waiterfn_timeout(void *arg)
{
	struct timespec tv;
	time_t timo, tnow;
	int ret;
	struct futex *f = (struct futex *) arg;

	int *p_ret = malloc(sizeof(int));

	clock_gettime(CLOCK_MONOTONIC, &tv);
	tnow = NSEC * tv.tv_sec + tv.tv_nsec;
	timo = tnow + ((unsigned long) NSEC * 2);

	printf("\nTesting timeout (%.3f seconds)... ", (float) (timo - tnow) / NSEC);

	ret = futex_wait(f->uaddr, 0, f->flags, &timo);
	if (ret != -1 && errno != ETIMEDOUT) {
		printf("ERROR ret %d errno %d", ret, errno);
	}

	printf("\n");
	*p_ret = errno;
	pthread_exit(p_ret);
}

/*
 * Test for simple wait/wake mechanism
 * Wait expecting value 0
 */
void run_simple_wait(struct results *r)
{
	struct futex f;
	f.val = 0;
	futex32 f32 = 0;

	printf("\nsize = 32\n");
	f.uaddr = &f32;
	f.flags = FUTEX_PRIVATE_SIZE_32;
	test_single_waiter(f, r);
}
/*
 * Test a waiter with timeout
 */
void run_timeout(struct results *r)
{
	struct timespec tv;
	long long int timo;
	time_t tnow;
	futex32 f = 0;
	int ret;

	printf("size tv %d %d\n", sizeof(tv.tv_sec), sizeof(tv.tv_nsec));
	clock_gettime(CLOCK_REALTIME, &tv);
	tnow = NSEC * tv.tv_sec + tv.tv_nsec;

	timo = tnow + ((unsigned long) NSEC * 2);

	printf("size %d\n", sizeof(timo));
	printf("timo %lld\n", timo);

	printf("size tv %d %d\n", sizeof(tv.tv_sec), sizeof(tv.tv_nsec));

	printf("\nTesting realtime timeout (%.3f seconds)... ", (float) (timo - tnow) / NSEC);

	ret = futex_wait(&f, 0, FUTEX_PRIVATE_SIZE_32 | FUTEX_CLOCKRT, &timo);
	if (ret == -1 && errno == ETIMEDOUT) {
		printf("OK\n");
		r->pass += 1;
	} else {
		printf("ERROR ret %d errno %d\n", ret, errno);
		r->error += 1;
	}
	
	clock_gettime(CLOCK_MONOTONIC, &tv);
	tnow = NSEC * tv.tv_sec + tv.tv_nsec;

	timo = tnow + ((unsigned long) NSEC * 2);

	printf("timo %lld\n", timo);

	printf("\nTesting monotic timeout (%.3f seconds)... ", (float) (timo - tnow) / NSEC);

	ret = futex_wait(&f, 0, FUTEX_PRIVATE_SIZE_32, &timo);
	if (ret == -1 && errno == ETIMEDOUT) {
		printf("OK\n");
		r->pass += 1;
	} else {
		printf("ERROR ret %d errno %d\n", ret, errno);
		r->error += 1;
	}
}

/*
 * Test sharead futexes
 * The other tests uses privates ones
 */
void run_shared_futex(struct results *r)
{
	struct futex f;
	f.val = 0;
	futex32 f32 = 0;

	printf("\nsize = 32\n");
	f.uaddr = &f32;
	f.flags = FUTEX_SIZE_32;
	test_single_waiter(f, r);
}

int main()
{
	struct results results;
	results.pass = 0;
	results.error = 0;

	printf("NR_wait %d wake %d\n", __NR_futex_wait, __NR_futex_wake);

	run_simple_wait(&results);
	run_timeout(&results);
	run_shared_futex(&results);

	printf("\nSummary: %u pass %u error (total %u)\n", results.pass,
	       results.error, results.pass + results.error);

	return 0;
}
