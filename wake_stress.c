#define _GNU_SOURCE
#include <sched.h>
#include <sys/syscall.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <stdatomic.h>
#include <linux/futex.h>
#include <pthread.h>
#include <sys/time.h>
#include <stdbool.h>
#include <errno.h>

#define SEC_INTERVAL 0.1

#define STACK_SIZE 32768

#define USEC_PER_MSEC	1000L

__suseconds_t *results;
atomic_uintptr_t counter;

pthread_barrier_t barrier_wakers;
pthread_barrier_t barrier_waiters;

uint32_t global_futex = 0;

uint32_t *waiters;
bool futex2 = true;

#define futex(uaddr, op, val, timo) \
	syscall(__NR_futex1, uaddr, op | flags, val, timo)

# define futex1_wake(uaddr, nr, flags) \
	futex(uaddr, FUTEX_WAKE, nr, NULL)

# define futex1_wait(uaddr, val, flags, timo) \
	futex(uaddr, FUTEX_WAIT, val, timo)

# define futex2_wake(uaddr, nr, flags) \
	syscall(__NR_futex_wake, uaddr, nr, flags)

# define futex2_wait(uaddr, val, flags, timo) \
	syscall(__NR_futex_wait, uaddr, val, flags, timo)

static int futex_wake(void *uaddr, unsigned int nr, unsigned int flags)
{
	if (futex2)
		return futex2_wake(uaddr, nr, flags | FUTEX_32);
	else
		return futex1_wake(uaddr, nr, flags | FUTEX_PRIVATE_FLAG);
}

static int futex_wait(void *uaddr, unsigned int val, unsigned int flags, void *timo)
{
	if (futex2)
		return futex2_wait(uaddr, val, flags | FUTEX_32, timo);
	else
		return futex1_wait(uaddr, val, flags | FUTEX_PRIVATE_FLAG, timo);
}

void *waker_fn(void *arg)
{
	struct timeval start, end, res;
	int i = (int *) arg;
	int ret;

	pthread_barrier_wait(&barrier_waiters);
	pthread_barrier_wait(&barrier_wakers);

	gettimeofday(&start, NULL);

	ret = futex_wake(&global_futex, 1, 0);

	gettimeofday(&end, NULL);
	timersub(&end, &start, &res);

	results[i] = res.tv_usec;

	return NULL;
}

void *waiter_fn(void *arg)
{
	uint32_t *futex = (uint32_t *) arg;
	pthread_barrier_wait(&barrier_waiters);

	futex_wait(futex, 0, 0, NULL);

	return NULL;
}

float wake_stress(int num_wakers, int num_waiters)
{
	int i;
	unsigned int total = 0;
	float avg;
	int num_total = num_wakers + num_waiters;

	pthread_t *threads = calloc(sizeof(pthread_t), num_total);

	waiters = calloc(sizeof(*waiters), num_waiters);
	results = malloc(sizeof(*results) * num_wakers);

	pthread_barrier_init(&barrier_waiters, NULL, num_total);
	pthread_barrier_init(&barrier_wakers, NULL, num_wakers);


	for (i = 0; i < num_waiters; i++) {
		pthread_create(&threads[i + num_wakers], NULL, waiter_fn, &waiters[i]);
	}

	/* allocate stacks and create childs */
	for (i = 0; i < num_wakers; i++) {
		uintptr_t a = i;
		pthread_create(&threads[i], NULL, waker_fn, (void *) a);
	}

	/* wait until everybody is done */
	for (i = 0; i < num_wakers; i++) {
		if (pthread_join(threads[i], NULL)) {
			perror("join");
			exit(1);
		}
		total += results[i];
	}

	/* get rid of waiters */
	for (i = 0; i < num_waiters; i++) {
		while (!futex_wake(&waiters[i], 1, 0));
	}

	for (i = num_wakers; i < num_total; i++) {
		if (pthread_join(threads[i], NULL)) {
			perror("join");
			exit(1);
		}
	}

	pthread_barrier_destroy(&barrier_wakers);
	pthread_barrier_destroy(&barrier_waiters);

	avg = total / (num_wakers * 1.0);

	return avg;
}

void print_help()
{
	printf("\t--csv: headerless csv output\n");
	printf("\t--futex1: use futex1 API\n");
	printf("\t--futex2: use futex1 API (default)\n");
	printf("\t--help: this message\n");
}

int main(int argc, char *argv[])
{
	int i, runs = 100, num_waiters = 0, num_wakers = 8;
	float avg, total = 0;
	bool csv = false;

	for (i = 1; i < argc; i++) {
		if (!strcmp(argv[i], "--csv"))
			csv = true;
		else if (!strcmp(argv[i], "--futex1"))
			futex2 = false;
		else if (!strcmp(argv[i], "--futex2"))
			continue;
		else if ((!strcmp(argv[i], "--runs") || !strcmp(argv[i], "-r")) && (i + 1 <= argc)) {
			runs = atoi(argv[++i]);
			if (runs <= 0)
				runs = 100;
		}
		else if ((!strcmp(argv[i], "--waiters") || !strcmp(argv[i], "-t")) && (i + 1 <= argc)) {
			num_waiters = atoi(argv[++i]);
			if (num_waiters <= 0)
				num_waiters = 0;
		}
		else if ((!strcmp(argv[i], "--wakers") || !strcmp(argv[i], "-k")) && (i + 1 <= argc)) {
			num_wakers = atoi(argv[++i]);
			if (num_wakers <= 0)
				num_wakers = 0;
		}
		else {
			print_help();
			return 0;
		}
	}

	if (!csv) {
		printf("wake stress. %d wakers (with %d waiters for extra stress)\n",
			num_wakers, num_waiters);

		printf("%d runs, using %s\n", runs, (futex2 ? "futex2" : "futex1"));
	}

	for (i = 0; i < runs; i++) {
		avg = wake_stress(num_wakers, num_waiters);
		total += avg;

		if (csv)
			printf("%.4f, %u, %u, %f, %u, %s\n", avg, num_wakers, num_waiters, runs, futex2 ? "2" : "1");
		else
			printf("RUN [%02d/%02d]: avg time %.4f\n", i + 1, runs, avg);


		sleep(SEC_INTERVAL);
	}

	if (!csv)
		printf("Average of %d runs: %.4f ms\n", runs, total / runs);

	return 0;
}
