#include <sched.h>
#include <sys/syscall.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <stdatomic.h>
#include <linux/futex.h>
#include <pthread.h>
#include <sys/time.h>
#include <stdbool.h>
#include <errno.h>

bool futex2 = true;

#define futex(uaddr, op, val, timo) \
	syscall(__NR_futex1, uaddr, op | flags, val, timo)

# define futex1_wake(uaddr, nr, flags) \
	futex(uaddr, FUTEX_WAKE, nr, NULL)

# define futex1_wait(uaddr, val, flags, timo) \
	futex(uaddr, FUTEX_WAIT, val, timo)

# define futex2_wake(uaddr, nr, flags) \
	syscall(__NR_futex_wake, uaddr, nr, flags)

# define futex2_wait(uaddr, val, flags, timo) \
	syscall(__NR_futex_wait, uaddr, val, flags, timo)

static int futex_wake(void *uaddr, unsigned int nr, unsigned int flags)
{
	if (futex2)
		return futex2_wake(uaddr, nr, flags | FUTEX_32);
	else
		return futex1_wake(uaddr, nr, flags | FUTEX_PRIVATE_FLAG);
}

static int futex_wait(void *uaddr, unsigned int val, unsigned int flags, void *timo)
{
	if (futex2)
		return futex2_wait(uaddr, val, flags | FUTEX_32, timo);
	else
		return futex1_wait(uaddr, val, flags | FUTEX_PRIVATE_FLAG, timo);
}

void *waiter_fn(void *arg)
{
	uint32_t futex = 0;
	int ret = futex_wait(&futex, 0, 0, 0);

	printf("ret %d\n", ret);

	return NULL;
}

int main()
{
	pthread_t thread;

	pthread_create(&thread, NULL, waiter_fn, NULL);

	sleep(2);
	
	return 0;
}

