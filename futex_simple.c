#include <errno.h>
#include <linux/unistd.h>
#include <pthread.h>
#include <stdint.h>
#include <stdio.h>
#include <unistd.h>

#define WAKE_WAIT_US 1000000
#define ERROR -1

/*
 * wait for uaddr if (*uaddr == val)
 */
static int futex_wait(void *uaddr, unsigned long val, unsigned long flags,
		      time_t *timo)
{
	return syscall(__NR_futex_wait, uaddr, val, flags, timo);
}

/*
 * wake nr futexes waiting for uaddr
 */
static int futex_wake(void *uaddr, unsigned int nr_wake, unsigned long flags)
{
	return syscall(__NR_futex_wake, uaddr, nr_wake, flags);
}

void *waiterfn(void *arg)
{
	int ret;

	ret = futex_wait(arg, 0, 0, NULL);
	if (ret == ERROR)
		printf("waiter failed %d errno %d\n", ret, errno);
	else
		printf("woke\n");

	return NULL;
}

int main()
{
	pthread_t waiter[3];
	uint32_t futex = 0;
	int ret;

	pthread_create(&waiter[0], NULL, waiterfn, &futex);
	pthread_create(&waiter[1], NULL, waiterfn, &futex);
	pthread_create(&waiter[2], NULL, waiterfn, &futex);

	usleep(WAKE_WAIT_US);

	ret = futex_wake(&futex, 3, 0);
	if (ret == 3)
		printf("OK\n");
	else
		printf("ERROR ret %d errno %d\n", ret, errno);

	usleep(WAKE_WAIT_US);

	return 0;
}
