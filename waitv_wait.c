#define _GNU_SOURCE
#include <sched.h>
#include <sys/syscall.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <stdatomic.h>
#include <linux/futex.h>
#include <pthread.h>
#include <sys/time.h>
#include <stdbool.h>
#include <errno.h>

#define SEC_INTERVAL 0.1

#define STACK_SIZE 32768

#define USEC_PER_MSEC	1000L

__suseconds_t *results;
atomic_uintptr_t counter;

pthread_barrier_t barrier_wakers;
pthread_barrier_t barrier_waiters;

uint32_t global_futex = 0;

uint32_t *waiters;
bool wait_multiple = true;

#define futex(uaddr, op, val, timo) \
	syscall(__NR_futex, uaddr, op | flags, val, timo)

#define FUTEX_WAIT_MULTIPLE 31

struct futex_wait_block {
	uint32_t *addr;
	int val;
};


static int futex_wait(void *uaddr, unsigned int val, unsigned int flags, void *timo)
{
	return futex(uaddr, FUTEX_WAIT, val, timo);

}

static int futex_wait_multiple(void *uaddr, unsigned int nr,
			       unsigned int flags, void *timo)
{
	return futex(uaddr, FUTEX_WAIT_MULTIPLE, nr, timo);
}

void *wait_fn(void *arg)
{
	struct timeval start, end, res;
	int i = (int *) arg;
	int ret;

	pthread_barrier_wait(&barrier_waiters);

	gettimeofday(&start, NULL);

	ret = futex_wait(&global_futex, global_futex + 1, FUTEX_PRIVATE_FLAG, NULL);

	gettimeofday(&end, NULL);
	timersub(&end, &start, &res);

	results[i] = res.tv_usec;

	return NULL;
}

void *wait_multiple_fn(void *arg)
{
	struct timeval start, end, res;
	int i = (int *) arg;
	struct futex_wait_block fwb;

	pthread_barrier_wait(&barrier_waiters);

	gettimeofday(&start, NULL);

	fwb.addr = &global_futex;
	fwb.val = global_futex + 1;

	futex_wait_multiple(&fwb, 1, FUTEX_PRIVATE_FLAG, NULL);

	gettimeofday(&end, NULL);
	timersub(&end, &start, &res);

	results[i] = res.tv_usec;

	return NULL;
}

float wake_stress(int num_waiters)
{
	int i;
	unsigned int total = 0;
	float avg;

	pthread_t *threads = calloc(sizeof(pthread_t), num_waiters);

	waiters = calloc(sizeof(*waiters), num_waiters);
	results = malloc(sizeof(*results) * num_waiters);

	pthread_barrier_init(&barrier_waiters, NULL, num_waiters);

	/* allocate stacks and create childs */
	for (i = 0; i < num_waiters; i++) {
		uintptr_t a = i;
		if (wait_multiple)
			pthread_create(&threads[i], NULL, wait_multiple_fn, (void *) a);
		else
			pthread_create(&threads[i], NULL, wait_fn, (void *) a);
	}

	/* wait until everybody is done */
	for (i = 0; i < num_waiters; i++) {
		if (pthread_join(threads[i], NULL)) {
			perror("join");
			exit(1);
		}
		total += results[i];
	}

	pthread_barrier_destroy(&barrier_waiters);

	avg = total / (num_waiters * 1.0);

	return avg;
}

void print_help()
{
	printf("\t--csv: headerless csv output\n");
	printf("\t--wait: use wait API\n");
	printf("\t--wait_multiple: use wait_multiple API (default)\n");
	printf("\t--waiters: number of waiters\n");
	printf("\t--runs: number of runs\n"):
	printf("\t--help: this message\n");
}

int main(int argc, char *argv[])
{
	int i, runs = 100, num_waiters = 8;
	float avg, total = 0;
	bool csv = false;

	for (i = 1; i < argc; i++) {
		if (!strcmp(argv[i], "--csv"))
			csv = true;
		else if (!strcmp(argv[i], "--wait"))
			wait_multiple = false;
		else if (!strcmp(argv[i], "--wait_multiple"))
			continue;
		else if ((!strcmp(argv[i], "--runs") || !strcmp(argv[i], "-r")) && (i + 1 <= argc)) {
			runs = atoi(argv[++i]);
			if (runs <= 0)
				runs = 100;
		}
		else if ((!strcmp(argv[i], "--waiters") || !strcmp(argv[i], "-t")) && (i + 1 <= argc)) {
			num_waiters = atoi(argv[++i]);
			if (num_waiters <= 0)
				num_waiters = 0;
		}
		else {
			print_help();
			return 0;
		}
	}

	if (!csv) {
		printf("wait stress %d waiters\n", num_waiters);

		printf("%d runs, using %s\n", runs, (wait_multiple ? "wait_multiple" : "wait"));
	}

	for (i = 0; i < runs; i++) {
		avg = wake_stress(num_waiters);
		total += avg;

		if (csv)
			printf("%.4f, %u, %u, %d\n", avg, num_waiters, runs, wait_multiple ? 2 : 1);
		else
			printf("RUN [%02d/%02d]: avg time %.4f\n", i + 1, runs, avg);


		sleep(SEC_INTERVAL);
	}

	if (!csv)
		printf("Average of %d runs: %.4f ms\n", runs, total / runs);

	return 0;
}
